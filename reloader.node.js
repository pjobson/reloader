#!/usr/bin/env node

const spawn     = require('child_process').spawn;
const exec      = require('child_process').exec;
const Q         = require('q');
const parseArgs = require('minimist')(process.argv);
const tilde     = require('tilde-expansion');
const which     = require('which');
const exit      = require('exit');

const reloader = {
	fswatch: false,
	browser: false,
	path:    false,
	init: function() {
		var rldr = this;
		if (!parseArgs.browser || !parseArgs.path) {
			rldr.usage();
		}
		rldr.browser = parseArgs.browser;
		tilde(parseArgs.path, function(s) {
			rldr.path = s;
		});
		var deferred = Q.defer();
		rldr.checkFSWATCH().then(function(chk) {
			if (chk.error) {
				console.log('\n'+'Missing fswatch executable or not in $PATH.'+'\n');
				rldr.usage();
			}
			rldr.fswatch = chk.resolvedPath;
		}).then(function() {
			rldr.fswatcher();
		});
	},
	checkFSWATCH: function() {
		var deferred = Q.defer();
		which('fswatch', function (error, resolvedPath) {
			deferred.resolve({
				error        : error,
				resolvedPath : resolvedPath
			});
		});
		return deferred.promise;
	},
	fswatcher: function() {
		var rldr = this;
		var fsw = spawn(rldr.fswatch, ['-o', rldr.path]);
		console.log('Waiting for changes on: '+ rldr.path);
		fsw.stdout.on('data', (data) => {
			console.log('Change Detected');
			rldr.osascript();
		});

		fsw.stderr.on('data', (data) => {
			console.log('stderr: ' + data.toString());
		});

		fsw.on('exit', (code) => {
			console.log('child process exited with code ' + code.toString());
		});


	},
	osascript: function() {
		var rldr = this;
		var osascript = "osascript -e 'tell application \""+ rldr.browser +"\" to tell the active tab of its first window to reload'";
		exec(osascript, (error, stdout, stderr) => {
			if (error) {
				console.error(`exec error: ${error}`);
				exit(0);
			}
			console.log('Reloading "'+ rldr.browser +'" active tab of top window.');
		});
	},
	usage: function() {
		var usage = [
			'Usage:',
			'./reloader.node.js --path=~/code/inote-ui --browser="Google Chrome"',
			'',
			'browser -- name of the browser you want to refresh, such as "Google Chrome" or Chromium',
			'path    -- path you want to watch',
			'',
			'You need fswatch to use this: brew install fswatch'
		];
		console.log(usage.join("\n"));
		exit(0);
	}
};

reloader.init();


// fswatch -o ~/code/inote-ui
// osascript -e 'tell application "Google Chrome Canary" to tell the active tab of its first window to reload'
