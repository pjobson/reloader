# reloader

	Usage:
	./reloader.node.js --path=~/code/inote-ui --browser="Google Chrome"
	browser -- name of the browser you want to refresh, such as "Google Chrome" or Chromium
	path    -- path you want to watch
	You need fswatch to use this: brew install fswatch
